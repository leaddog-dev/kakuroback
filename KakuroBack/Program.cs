var builder = WebApplication.CreateBuilder(args);
var  originsAllowed = "_originsAllowed";

// Add services to the container.
var corsOrigins = new []{"http://localhost:8081"};
// Add CORS origins
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: originsAllowed,
        policy =>
        {
            policy.WithOrigins(corsOrigins);
            policy.AllowAnyHeader();
            policy.AllowAnyMethod();
        });
});
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.UseOneOfForPolymorphism();
    c.UseAllOfForInheritance();
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

// Add CORS configuration
app.UseCors(originsAllowed);

app.UseAuthorization();

app.MapControllers();

app.Run();