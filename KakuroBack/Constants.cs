namespace KakuroBack;

public static class CellType
{
    public const string Input = "input";
    public const string Sum = "sum";
    public const string Blocked = "blocked";
    public const string Black = "black";
}