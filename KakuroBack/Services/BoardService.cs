using KakuroBack.Models;
using Microsoft.AspNetCore.Razor.Hosting;
using static System.Int32;

namespace KakuroBack.Services;

public class BoardService
{

    public IList<Row> GenerateBoardWithSums(int width, int height)
    {
        IList<Row> rowList;
        do
        {
            rowList = GenerateBoardWithSumCells(width, height);
            GenerateSums(rowList);
            GenerateInputCells(rowList);
        } while (InvalidBoard(rowList));

        return rowList;
    }

    private bool InvalidBoard(IList<Row> rowList)
    {
        for (var i = 0; i < rowList.Count; i++)
        {
            var cellList = rowList[i].Cells;
            for (var j = 0; j < cellList.Count; j++)
            {
                if (!cellList[j].Type.Equals(CellType.Input)) continue;

                var inputCell = (InputCell)cellList[j];
                // Check if there's any possible numbers
                if (inputCell.PossibleNumbers.Count == 0)
                {
                    return true;
                }
                // Check the vertical neighbours if it doesn't have any horizontal SumCell 
                if (inputCell.HorizontalSumCell == null)
                {
                    // There's always a row above for input cells
                    if (rowList[i - 1].Cells[j].Type.Equals(CellType.Input)
                        && ((InputCell)rowList[i - 1].Cells[j]).HorizontalSumCell == null)
                    {
                        return true;
                    }
                    // There might not be a row below
                    if ((i + 1) < rowList.Count && rowList[i + 1].Cells[j].Type.Equals(CellType.Input)
                                                && ((InputCell)rowList[i + 1].Cells[j]).HorizontalSumCell == null)
                    {
                        return true;
                    }
                }
                // Check the horizontal neighbours if it doesn't have any vertical SumCell
                if (inputCell.VerticalSumCell == null)
                {
                    // There's always a column before for input cells
                    if (cellList[j-1].Type.Equals(CellType.Input)
                        && ((InputCell)cellList[j-1]).VerticalSumCell == null)
                    {
                        return true;
                    }
                    // There might not be a column after
                    if ((j + 1) < cellList.Count && cellList[j + 1].Type.Equals(CellType.Input)
                                                && ((InputCell)cellList[j + 1]).VerticalSumCell == null)
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }
    private void GenerateInputCells(IList<Row> rowList)
    {
        for (var i = 0; i < rowList.Count; i++)
        {
            var cellList = rowList[i].Cells;
            for (var j = 0; j < cellList.Count; j++)
            {
                if (!cellList[j].Type.Equals(CellType.Input)) continue;

                var inputCell = (InputCell)cellList[j];
                // Get horizontal sum cell
                var idx = j - 1;
                while (idx >= 0 && cellList[idx].Type.Equals(CellType.Input))
                {
                    idx--;
                }
                inputCell.HorizontalSumCell = cellList[idx].Type.Equals(CellType.Sum)? ((SumCell)cellList[idx]) : null;
                // Get vertical sum cell
                idx = i - 1;
                while (idx >= 0 && rowList[idx].Cells[j].Type.Equals(CellType.Input))
                {
                    idx--;
                }
                inputCell.VerticalSumCell = rowList[idx].Cells[j].Type.Equals(CellType.Sum)
                    ? ((SumCell)rowList[idx].Cells[j])
                    : null;
                inputCell.PossibleNumbers = GetPossibleNumbers(
                    inputCell.HorizontalSumCell?.TopSum, 
                    inputCell.VerticalSumCell?.BottomSum);
            }
        }
    }

    private List<int> GetPossibleNumbers(Sum? horizontalSum, Sum? verticalSum)
    {
        List<int> GetNumbers(Sum? sum)
        {
            var numberList = new List<int>();
            if (sum != null)
            {
                foreach (var alternative in sum.Alternatives)
                {
                    foreach (var c in alternative.ToCharArray())
                    {
                        int alt;
                        if (TryParse(c.ToString(), out alt))
                        {
                            if (!numberList.Contains(alt))
                            {
                                numberList.Add(alt);
                            }
                        }
                    }
                }
            }
            return numberList;
        }

        var numberList = GetNumbers(horizontalSum);
        var verticalNumbers = GetNumbers(verticalSum);
        if (numberList.Count > 0 && verticalNumbers.Count > 0)
        {
            numberList = numberList.Intersect(verticalNumbers).ToList();
        }
        else if (numberList.Count == 0)
        {
            numberList = verticalNumbers;
        }
        
        return numberList.OrderBy(num => num).ToList();
    }
    
    private IList<Row> GenerateSums(IList<Row> rowList)
    {
        var sumSolutions = GenerateSumSolutions();
        
        for (var i = 0; i < rowList.Count; i++)
        {
            var cellList = rowList[i].Cells;
            for (var j = 0; j < cellList.Count; j++)
            {
                if (!cellList[j].Type.Equals(CellType.Sum)) continue;

                var sumCell = (SumCell)cellList[j];
                if (sumCell.BottomLength.HasValue)
                {
                    sumCell.PossibleBottomSums = GetPossibleSums(sumCell.BottomLength.Value, sumSolutions);
                    if (sumCell.PossibleBottomSums.Count == 1)
                    {
                        sumCell.BottomSum = sumCell.PossibleBottomSums[0];
                        sumCell.BottomNumber = sumCell.BottomSum.Value;
                    }
                    else
                    {
                        var selector = new Random().Next(0, sumCell.PossibleBottomSums.Count);
                        sumCell.BottomSum = sumCell.PossibleBottomSums[selector];
                        sumCell.BottomNumber = sumCell.BottomSum.Value;
                    }
                }

                if (sumCell.TopLength.HasValue)
                {
                    sumCell.PossibleTopSums = GetPossibleSums(sumCell.TopLength.Value, sumSolutions);
                    if (sumCell.PossibleTopSums.Count == 1)
                    {
                        sumCell.TopSum = sumCell.PossibleTopSums[0];
                        sumCell.TopNumber = sumCell.TopSum.Value;
                    }
                    else
                    {
                        var selector = new Random().Next(0, sumCell.PossibleTopSums.Count);
                        sumCell.TopSum = sumCell.PossibleTopSums[selector];
                        sumCell.TopNumber = sumCell.TopSum.Value;
                    }
                }
            }
        }

        return rowList;
    }

    private List<Sum> GetPossibleSums(int length, IList<Sum> sumSolutions)
    {
        return sumSolutions.Where(sum => sum.Length == length).ToList();
    }

    public IList<Row> GenerateBoardWithSumCells(int width, int height)
    {
        var rowList = GenerateBoard(width, height);
        
        for (var i=0; i<rowList.Count; i++)
        {
            var cellList = rowList[i].Cells;
            for (var j=0; j<cellList.Count; j++)
            {
                // Only look at black cells
                if (!cellList[j].Type.Equals(CellType.Black)) continue;
                // Create sum cell, and calculate the length of fields
                var cell = new SumCell { Id = cellList[j].Id };
                if (j + 1 < cellList.Count && cellList[j+1].Type.Equals(CellType.Input))
                {
                    var topLength = 1;
                    var idx = j + 2;
                    while (idx < cellList.Count && cellList[idx].Type.Equals(CellType.Input))
                    {
                        topLength++;
                        idx++;
                    }
                    if (topLength > 1)
                    {
                        cell.TopNumber = topLength;
                        cell.TopLength = topLength;
                    }
                }

                if (i + 1 < rowList.Count && rowList[i + 1].Cells[j].Type.Equals(CellType.Input))
                {
                    var bottomLength = 1;
                    var idx = i + 2;
                    while (idx < rowList.Count && rowList[idx].Cells[j].Type.Equals(CellType.Input))
                    {
                        bottomLength++;
                        idx++;
                    }

                    if (bottomLength > 1)
                    {
                        cell.BottomNumber = bottomLength;
                        cell.BottomLength = bottomLength;
                    }
                }

                if (cell.BottomNumber.HasValue || cell.TopNumber.HasValue)
                {
                    cellList[j] = cell;
                }
            }
        }

        return rowList;
    }
    
    /// <summary>
    /// Generates a board. Parameters width and height should not include first row and column 
    /// </summary>
    /// <param name="width">Width of board, excluding first column</param>
    /// <param name="height">Height of board, excluding first row</param>
    /// <returns>A board in the form of a list of Row objects</returns>
    public IList<Row> GenerateBoard(int width, int height)
    {
        var rowList = new List<Row>();
        var halfHeight = height/2;
        rowList.Add(GetFirstRow(width));
        for (var i = 1; i <= halfHeight; i++)
        {
            rowList.Add( new Row { Cells = GetCellList(width, i) });
        }
        // Do we have an uneven height?
        if (height % 2 == 1)
        {
            rowList.Add( new Row { Cells = GetCenterCellList(width, rowList.Count) } );
        }
        // Set the index to use when reversing the other half of rows
        var rowIndex = rowList.Count;
        // Reverse the current cell lists of the existing rows
        for(var i = halfHeight; i >= 1; i--)
        {
            rowList.Add( new Row { Cells = GetReversedCellList((List<Cell>)rowList[i].Cells, rowIndex++)});
        }

        return rowList;
    }
    
    public IList<Sum> GenerateSumSolutions()
    {
        var sums = new List<Sum>();
        for (var i = 3; i <= 45; i++)
        {
            for (var j = 2; j <= 9; j++)
            {
                var sum = new Sum
                {
                    Id = i + "/" + j,
                    Value = i,
                    Length = j,
                    Alternatives = GetAlternatives(
                        new []{ 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                        i,j, 0, "")
                };
                
                if (sum.Alternatives.Count > 0)
                {
                    sums.Add(sum);
                }
            }
        }

        return sums;
    }

    private Row GetFirstRow(int width)
    {
        var cellList = new List<Cell>();
        for (var i = 0; i <= width; i++)
        {
            cellList.Add(new BlackCell{Id = "0-" + (i)});
        }

        return new Row { Cells = cellList };
    }
    
    private IList<Cell> GetCellList(int width, int rowId)
    {
        var widthList = new List<int>();
        var maxFieldWidth = width <= 9 ? width : 9;
        var fieldWidth = new Random().Next(2, maxFieldWidth+1);
        var sumWidth = fieldWidth;
        while (sumWidth <= width)
        {
            widthList.Add(fieldWidth);
            if (maxFieldWidth-sumWidth-1 > 2)
            {
                fieldWidth = new Random().Next(2, maxFieldWidth-sumWidth-1);
            }
            sumWidth += fieldWidth + 1;
        }

        var cellList = new List<Cell>();
        var cellCounter = 0;
        cellList.Add(new BlackCell {Id = rowId + "-" + cellCounter++});
        foreach (var w in widthList)
        {
            for (var j = 0; j < w; j++)
            {
                cellList.Add(new InputCell {Id = rowId + "-" + cellCounter++});
            }
            cellList.Add(new BlackCell {Id = rowId + "-" + cellCounter++});
        }

        if (cellList.Count > width + 1)
        {
            cellList.RemoveRange(width+1, cellList.Count - width -1);
        }
        while (cellList.Count <= width )
        {
            cellList.Add(new BlackCell {Id = rowId + "-" + cellCounter++});
        }

        return cellList;
    }
    
    private IList<Cell> GetCenterCellList(int width, int rowId)
    {
        var halfWidth = width / 2;
        var maxFieldWidth = width <= 9 ? width : 9;
        var fieldWidth = new Random().Next(2, maxFieldWidth+1);
        var cellList = new List<Cell>();
        // If field is bigger than half
        if (fieldWidth > halfWidth)
        {
            var rest = width - fieldWidth;
            if (rest % 2 > 0)
            {
                rest--;
                fieldWidth++;
            }
            
            for (var i = 1; i <= rest / 2; i++)
            {
                cellList.Add(new BlackCell{Id = rowId + "-" + i});
            }

            var count = cellList.Count;
            for (var i = count + 1; i <= count + fieldWidth; i++)
            {
                cellList.Add(new InputCell{Id = rowId + "-" + i});
            }
            
            for (var i = cellList.Count + 1; i <= width; i++)
            {
                cellList.Add(new BlackCell{Id = rowId + "-" + i});
            }
            
        }
        else
        {
            var rest = width - 2*fieldWidth;
            if (rest % 2 != 1)
            {
                rest--;
            }
            for (var i = 1; i <= rest / 2; i++)
            {
                cellList.Add(new BlackCell{Id = rowId + "-" + i});
            }

            var count = cellList.Count;
            for (var i = count + 1; i <= count + fieldWidth; i++)
            {
                cellList.Add(new InputCell{Id = rowId + "-" + i});
            }
            cellList.Add(new BlackCell{Id = rowId + "-" + (cellList.Count+1)});
            count = cellList.Count;
            for (var i = count + 1; i <= count + fieldWidth; i++)
            {
                cellList.Add(new InputCell{Id = rowId + "-" + i});
            }
            
            for (var i = cellList.Count + 1; i <= width; i++)
            {
                cellList.Add(new BlackCell{Id = rowId + "-" + i});
            }
        }
        // Add the first cell
        cellList.Insert(0,new BlackCell{Id = rowId + "-0"});

        return cellList;
    }

    private List<Cell> GetReversedCellList(List<Cell> cells, int rowId)
    {
        var reverseList = new List<Cell>();
        reverseList.Add(new BlackCell());
        for (var i = cells.Count-1; i > 0; i--)
        {
            if (cells[i].Type.Equals(CellType.Black))
            {
                reverseList.Add(new BlackCell());
            }
            else
            {
                reverseList.Add(new InputCell());
            }
        }
        for (var i = 0; i < reverseList.Count; i++)
        {
            reverseList[i].Id = rowId + "-" + i;
        }

        return reverseList;
    }

    private IList<string> GetAlternatives(int[] numbers, int sum, int length, int partSum, string alternativeStart)
    {
        var alternatives = new List<string>();
        var remainingNumbers = numbers;
        foreach (var number in numbers)
        {
            var alternativeSum = partSum + number;
            var alternative = alternativeStart + number;
            remainingNumbers = remainingNumbers.Skip(1).ToArray();
            if (alternativeSum == sum && alternative.Length == length)
            {
                alternatives.Add(alternative);
            }
            else if (alternativeSum > sum)
            {
                break;
            }
            else if (alternative.Length < length)
            {
                alternatives.AddRange(GetAlternatives(remainingNumbers, 
                    sum, length, alternativeSum, alternative));
            }
        }

        return alternatives;
    }
    
}