namespace KakuroBack.Models;

public class InputCell: Cell
{
    public override string Type => CellType.Input;
    public string Solution { get; set; }
    public string Value { get; set; } = string.Empty;
    public string State { get; set; } = "empty";
    public bool Validated { get; set; } = false;
    public List<int> PossibleNumbers { get; set; } = new (){1, 2, 3, 4, 5, 6, 7, 8, 9};
    public SumCell? VerticalSumCell { get; set; }
    public SumCell? HorizontalSumCell { get; set; }
}