namespace KakuroBack.Models;

public class ObjectRow
{
    public IList<object> Cells { get; set; } = new List<object>();
}