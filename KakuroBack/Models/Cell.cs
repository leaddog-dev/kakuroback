namespace KakuroBack.Models;

//[SwaggerSubType(typeof(SumCell))]
public abstract class Cell
{
    public string Id { get; set; }
    public abstract string Type { get; }
}