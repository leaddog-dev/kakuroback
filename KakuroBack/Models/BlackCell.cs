namespace KakuroBack.Models;

public class BlackCell: Cell
{
    public override string Type => CellType.Black;
}