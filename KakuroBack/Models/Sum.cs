namespace KakuroBack.Models;

public class Sum
{
    public string Id { get; set; }
    public int Value { get; set; }
    public int Length { get; set; }
    public IList<string> Alternatives { get; set; } = new List<string>();
}