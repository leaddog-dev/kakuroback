namespace KakuroBack.Models;

public class BlockedCell: Cell
{
    public override string Type => CellType.Blocked;
}