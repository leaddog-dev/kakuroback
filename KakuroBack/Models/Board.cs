namespace KakuroBack.Models;

public class Board
{
    public int NumberOfRows { get; set; }
    public int NumberOfColumns { get; set; }
    public string Size => NumberOfRows + "x" + NumberOfColumns;

    public IList<Row> Rows { get; set; } = new List<Row>();
}