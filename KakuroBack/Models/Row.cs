namespace KakuroBack.Models;

public class Row
{
    public IList<Cell> Cells { get; set; } = new List<Cell>();
}