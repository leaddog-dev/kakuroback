namespace KakuroBack.Models;

public class SumCell: Cell
{
    public override string Type => CellType.Sum;
    // Horizontal sum
    public int? TopNumber { get; set; }
    public Sum? TopSum { get; set; }
    // Vertical sum
    public int? BottomNumber { get; set; }
    public Sum? BottomSum { get; set; }
    public int? TopLength { get; set; }
    public int? BottomLength { get; set; }
    public List<Sum> PossibleTopSums { get; set; } = new();
    public List<Sum> PossibleBottomSums { get; set; } = new();
}