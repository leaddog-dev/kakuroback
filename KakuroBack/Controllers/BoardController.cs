using System.Text.Json;
using KakuroBack.Models;
using KakuroBack.Services;
using Microsoft.AspNetCore.Mvc;

namespace KakuroBack.Controllers;

[ApiController]
[Route("[controller]")]
[Produces("application/json")]
public class BoardController: ControllerBase
{
    
    [HttpGet("Sums")]
    public IList<Sum> Sums()
    {
        var service = new BoardService();
        return service.GenerateSumSolutions();
    }
    
    [HttpGet("JustBoard")]
    public IList<Row> JustBoard(int width, int height)
    {
        var service = new BoardService();
        return service.GenerateBoardWithSumCells(width, height);
    }
    
    [HttpGet]
    public IList<ObjectRow> GetBoard(int width, int height)
    {
        var service = new BoardService();
        var list = service.GenerateBoardWithSums(width, height);
        var newList = new List<ObjectRow>();
        foreach (var item in list)
        {
            var cellList = new List<object>();
            foreach (var cell in item.Cells)
            {
                cellList.Add(cell);
            }
            newList.Add(new ObjectRow {Cells = cellList});
        }
        
        /*var options = new JsonSerializerOptions
        {
            WriteIndented = true
        };
        var jsonResult = new JsonResult(JsonSerializer.Serialize(newList, options));        
        return jsonResult;*/
        return newList;
    }
    
}